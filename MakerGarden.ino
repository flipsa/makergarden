//USER_LIB_PATH = /home/$(USER)/Arduino/libraries

/** Watchdog **/
#include <avr/wdt.h>

/** SoftwareReset **/
#include <SoftwareReset.h>
unsigned long previousResetTimer = 0;
unsigned long resetTimer = 3600000;

/** Libraries needed for Arduino Menu
 *  Depends on EEPROM for storing menu settings
(including rotary encoder) **/
#include <Arduino.h>
#include <menu.h>
#include <TimerOne.h>
#include <menuIO/clickEncoderIn.h>
#include <menuIO/chainStream.h>
#include <menuIO/serialOut.h>
#include <menuIO/serialIn.h>
#include <menuIO/U8x8Out.h>
using namespace Menu;
#include <EEPROM.h>
/** -------------------------------------------------------------------------- **/


/** Initialize the OLED  **/
U8X8_SH1106_128X64_NONAME_HW_I2C u8x8(A5, A4, U8X8_PIN_NONE);
/** -------------------------------------------------------------------------- **/


/** Rotary Encoder with Button  **/
#define encA A1
#define encB A0
#define encBtn A2
ClickEncoder clickEncoder(encA,encB,encBtn,4);
ClickEncoderStream encStream(clickEncoder,1);
/** -------------------------------------------------------------------------- **/


/** Menu Inputs  **/
serialIn serial(Serial); //changed on version 4
MENU_INPUTS(in,&encStream,&serial);
//MENU_INPUTS(in,&encStream);
void timerIsr() {clickEncoder.service();}


/** Menu settings variables **/
#define MAX_DEPTH 4
// Below are the menu settings that go into the settingsArray[].
byte selFanMode=0;
byte selPumpMode=0;
byte selLightMode=0;
// CAUTION with leading zeros: https://www.arduino.cc/reference/en/language/variables/constants/integerconstants/
byte autoLightStartTimeHour = 0;
byte autoLightEndTimeHour = 0;
byte autoLightStartTimeMinute = 0;
byte autoLightEndTimeMinute = 0;
byte tempMaximum;
byte humidityMaximum;
byte selMoistureModePot01=0;
byte moistureOptimumPot01;
byte selMoistureModePot02=0;
byte moistureOptimumPot02;
byte selMoistureModePot03=0;
byte moistureOptimumPot03;
byte selMoistureModePot04=0;
byte moistureOptimumPot04;
bool SERIALDEBUG=false;
/** -------------------------------------------------------------------------- **/

bool CLEANINGMODE=false;

/** EEPROM / NON-VOLATILE STORAGE **/
int settingsArray[18]; // We need to specify the array length here. Each element must be of type "byte"!!
int eepromAddr = 0; // Start writing to the EEPROM at the first address
/** -------------------------------------------------------------------------- **/


/**  Plants Array **/
int plantsArray[4];
/** -------------------------------------------------------------------------- **/


/** RealTimeClock **/
#include "RTClib.h"
RTC_DS1307 rtc;
char dateNow[20];
char dateNowShort[17];
int dateToday;
int today;
int daysRunning = 0;
/** -------------------------------------------------------------------------- **/


/** DHT Temperature & Humidity Sensor **/
#include <Adafruit_Sensor.h>
#include <DHT.h>
#include <DHT_U.h>
#define DHTPIN 25
#define DHTTYPE DHT22
DHT_Unified dht(DHTPIN, DHTTYPE);
//uint32_t delayDHT22;
float valueTemperature01;
float valueHumidity01;
/** ------------------------------------------------------------------------------------ **/


/** LEDs **/
#include <math.h> // needed for the LED breathe function
#define ledWater 10 // blue (pump)
#define ledClimate 11 // green (humidity)
#define ledLight 12 // yellow (light)
#define ledWarn 13 // red (temperature)
#define ledIrrigationPlant1 7 // blue (irrigation mode of plant 1)
#define ledIrrigationPlant2 6 // blue (irrigation mode of plant 2)
#define ledIrrigationPlant3 5 // blue (irrigation mode of plant 3)
#define ledIrrigationPlant4 4 // blue (irrigation mode of plant 4)
byte led;
byte ledIrrigationPlantX;
unsigned long ledPreviousMillis = 0;        // will store last time LED was updated
const long ledBlinkInterval = 250; // in ms
uint8_t ledState;
/** -------------------------------------------------------------------------- **/


/** Pump **/
#define relay1 22
unsigned long relay1CameOnX = 0;
unsigned long relay1CameOff = 0;
unsigned long autoIrrigationCounter = 0;
long autoIrrigationNextRun;
unsigned long irrigationWaitPeriod = 1800000; // 1 hour (in milliseconds)
unsigned int irrigationDuration = 2000; //
bool pumpIsOn = false;
bool pumpManual = false;
bool runPump;
int relayX;
/** -------------------------------------------------------------------------- **/


/** Fan **/
#define relay2 24
#define fanSpeedPin A3
byte fanSpeed;
unsigned long relay2CameOn = 0;
/** -------------------------------------------------------------------------- **/


/** Light **/
#define relay3 34
/** -------------------------------------------------------------------------- **/


/** Valve 1 **/
#define valve1 26
bool valve1Open = false;
/** -------------------------------------------------------------------------- **/


/** Valve 2 **/
#define valve2 28
bool valve2Open = false;
/** -------------------------------------------------------------------------- **/


/** Valve 3 **/
#define valve3 30
bool valve3Open = false;
/** -------------------------------------------------------------------------- **/


/** Valve 4 **/
#define valve4 32
bool valve4Open = false;
/** -------------------------------------------------------------------------- **/


/** valveX **/
byte valveX = 99;
uint8_t valveXState;
unsigned int valvesOpen = 0;
unsigned int dryPlants = 0;
/** -------------------------------------------------------------------------- **/


/** Capacitive Soil Moisture Sensor v1.2 **/
#define soilMoistureSensor1Pin A8
#define soilMoistureSensor2Pin A9
#define soilMoistureSensor3Pin A10
#define soilMoistureSensor4Pin A11

//const int airValue = 550;    // Sensor completely dry held up in the air. IMPORTANT: this is for external power. with USB power it's ~640;
const int airValue = 815; // while on non-USB (= VIn) power supply
//const int waterValue = 270;  // Sensor submerged in glas of water (as deep as allowed). IMPORTANT: this is for external power. with USB power it's ~310
const int waterValue = 380; // while on non-USB (=VIn) power supply
const int intervals = (airValue - waterValue)/3;
const float soilMoistureCoefficient = 4.35;

float percentSoilMoistureX;
float moistureOptimumPotX;
int selMoistureModePotX;
int soilMoistureValueX;
//int soilMoistureValue = 0; // Needed for the old non-functioning but cleaner method. Need to FIX readSoilMoisture()
int soilMoistureValue01;
float percentSoilMoisture01;
int soilMoistureValue02;
float percentSoilMoisture02;
int soilMoistureValue03;
float percentSoilMoisture03;
int soilMoistureValue04;
float percentSoilMoisture04;
/** -------------------------------------------------------------------------- **/


/** GY-30 Light Sensor **/
#include <Wire.h>
const byte gy30ICAdress = 0x23; // Alternative: 0x5C
byte buffer[2]; //Array für das "speichern" von gelesenen Werten
unsigned int valueBrightness01;
/** -------------------------------------------------------------------------- **/


/** Water Level Sensor **/
#define waterStatePin 23
bool hasWater;
/** -------------------------------------------------------------------------- **/


/** Water flow sensor: YF- S201 Hall Effect Water Flow Sensor
 *  http://www.theorycircuit.com/water-flow-sensor-yf-s201-arduino-interface/ **/
#define flowSensorPin 2
volatile int flow_frequency; // Measures flow sensor pulses
unsigned int l_hour; // Calculated litres/hour
unsigned long currentTime;
unsigned long cloopTime;
float waterTotal = 0.0;
float wpdTotal = 0.0;
int lphTotal = 0;
float waterValveX = 0.0;
float wpdValveX = 0.0;
float waterValve1 = 0.0;
int lphValve1 = 0;
float wpdValve1 = 0.0;
float waterValve2 = 0.0;
int lphValve2 = 0;
float wpdValve2 = 0.0;
float waterValve3 = 0.0;
int lphValve3 = 0;
float wpdValve3 = 0.0;
float waterValve4 = 0.0;
int lphValve4 = 0;
float wpdValve4 = 0.0;
/** -------------------------------------------------------------------------- **/


// Button1
#define button1Pin 46


// Button2
#define button2Pin 48


// Button3
#define button3Pin 50


// Button4
#define button4Pin 52


// Button5
#define button5Pin 38
String button5State = "OFF";
long button5Timer = 0;
int button5LongPressTime = 1000; // in milliseconds
boolean button5Active = false;
boolean button5LongPressActive = false;


// Button6
#define button6Pin 40
String button6State = "OFF";
long button6Timer = 0;
int button6LongPressTime = 1000; // in milliseconds
boolean button6Active = false;
boolean button6LongPressActive = false;


// Button7
#define button7Pin 42
String button7State = "OFF";
long button7Timer = 0;
int button7LongPressTime = 1000; // in milliseconds
boolean button7Active = false;
boolean button7LongPressActive = false;


// Button8
#define button8Pin 44
String button8State = "OFF";
long button8Timer = 0;
int button8LongPressTime = 1000; // in milliseconds
boolean button8Active = false;
boolean button8LongPressActive = false;


/** ButtonX **/
int buttonX;
bool buttonXActive;
bool buttonXLongPressActive;
bool momentaryButtonXActive;
bool momentaryButton1Active;
bool momentaryButton2Active;
bool momentaryButton3Active;
bool momentaryButton4Active;
/** -------------------------------------------------------------------------- **/


/** Loop interval and timing  **/
const int mainLoopIntervalSlow = 2000;
unsigned long previousMillis1 = 0;
unsigned long previousMillis2 = 0;
unsigned long statusScreenMillis = 0;
/** -------------------------------------------------------------------------- **/


/** EL-Client **/
//#include <ELClientSocket.h>
//#include <ELClientWebServer.h>
#include <ELClientCmd.h>
//#include <ELClientResponse.h>
//#include <FP.h>
#include <ELClientMqtt.h>
#include <ELClient.h>
#include <ELClientRest.h>
// Initialize a connection to esp-link using the normal hardware serial port both for
// SLIP and for debug messages.
//ELClient esp(&Serial, &Serial);
ELClient esp(&Serial);

// Initialize a REST client on the connection to esp-link
ELClientRest rest(&esp);

// Initialize CMD client (for GetTime)
ELClientCmd cmd(&esp);

// Initialize the MQTT client
ELClientMqtt mqtt(&esp);

boolean wifiConnected = false;
boolean connected = false;
unsigned long last = 0;
unsigned int mqttPublishInterval = 1000;

/** -------------------------------------------------------------------------- **/


/** Menu Structure **/
SELECT(selLightMode,selLightModeMenu,"Mode:",doNothing,noEvent,noStyle
  ,VALUE("AUTO",0,doNothing,noEvent)
  ,VALUE("OFF",1,doNothing,noEvent)
  ,VALUE("ON",2,doNothing,noEvent)
);

//define a pad style menu (single line menu)
//here with a set of fields to enter a date in YYYY/MM/DD format
altMENU(menu,autoLightStartTime,"From: ",doNothing,noEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(autoLightStartTimeHour,"",":",0,23,1,0,doNothing,noEvent,noStyle)
  ,FIELD(autoLightStartTimeMinute,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
);

//define a pad style menu (single line menu)
//here with a set of fields to enter a date in YYYY/MM/DD format
altMENU(menu,autoLightEndTime,"To: ",doNothing,noEvent,noStyle,(systemStyles)(_asPad|Menu::_menuData|Menu::_canNav|_parentDraw)
  ,FIELD(autoLightEndTimeHour,"",":",0,23,1,0,doNothing,noEvent,noStyle)
  ,FIELD(autoLightEndTimeMinute,"","",0,59,10,1,doNothing,noEvent,wrapStyle)
);

MENU(subMenuLight,"Light",doNothing,noEvent,noStyle
  ,SUBMENU(selLightModeMenu)
  //,FIELD(autoLightStartTimeHour,"Start:",":00",0,23,1,0,doNothing,noEvent,wrapStyle)
  //,FIELD(autoLightEndTimeHour,"Stop:",":00",0,23,1,0,doNothing,noEvent,wrapStyle)
  ,SUBMENU(autoLightStartTime)
  ,SUBMENU(autoLightEndTime)
  ,EXIT("<Back")
);

SELECT(selFanMode,selFanModeMenu,"Mode:",doNothing,noEvent,noStyle
  ,VALUE("AUTO",0,doNothing,noEvent)
  ,VALUE("OFF",1,doNothing,noEvent)
  ,VALUE("ON",2,doNothing,noEvent)
);

SELECT(selPumpMode,selPumpModeMenu,"Mode:",doNothing,noEvent,noStyle
  ,VALUE("AUTO",0,doNothing,noEvent)
  ,VALUE("OFF",1,doNothing,noEvent)
  ,VALUE("ON",2,doNothing,noEvent)
);

SELECT(selMoistureModePot01,selMoistureModeMenuPot01,"Mode:",doNothing,noEvent,noStyle
  ,VALUE("AUTO",0,doNothing,noEvent)
  ,VALUE("MANUAL",1,doNothing,noEvent)
);

MENU(subMenuPot01,"Pot #1",doNothing,noEvent,noStyle
  ,SUBMENU(selMoistureModeMenuPot01)
  ,FIELD(percentSoilMoisture01,"Current:","%",0,100,0,0,doNothing,noEvent,wrapStyle)
  ,FIELD(moistureOptimumPot01,"Optimum:","%",0,100,10,0,doNothing,noEvent,wrapStyle)
  ,EXIT("<Back")
);

SELECT(selMoistureModePot02,selMoistureModeMenuPot02,"Mode:",doNothing,noEvent,noStyle
  ,VALUE("AUTO",0,doNothing,noEvent)
  ,VALUE("MANUAL",1,doNothing,noEvent)
);

MENU(subMenuPot02,"Pot #2",doNothing,noEvent,noStyle
  ,SUBMENU(selMoistureModeMenuPot02)
  ,FIELD(percentSoilMoisture02,"Current:","%",0,100,0,0,doNothing,noEvent,wrapStyle)
  ,FIELD(moistureOptimumPot02,"Optimum:","%",0,100,10,0,doNothing,noEvent,wrapStyle)
  ,EXIT("<Back")
);

SELECT(selMoistureModePot03,selMoistureModeMenuPot03,"Mode:",doNothing,noEvent,noStyle
  ,VALUE("AUTO",0,doNothing,noEvent)
  ,VALUE("MANUAL",1,doNothing,noEvent)
);

MENU(subMenuPot03,"Pot #3",doNothing,noEvent,noStyle
  ,SUBMENU(selMoistureModeMenuPot03)
  ,FIELD(percentSoilMoisture03,"Current:","%",0,100,0,0,doNothing,noEvent,wrapStyle)
  ,FIELD(moistureOptimumPot03,"Optimum:","%",0,100,10,0,doNothing,noEvent,wrapStyle)
  ,EXIT("<Back")
);

SELECT(selMoistureModePot04,selMoistureModeMenuPot04,"Mode:",doNothing,noEvent,noStyle
  ,VALUE("AUTO",0,doNothing,noEvent)
  ,VALUE("MANUAL",1,doNothing,noEvent)
);

MENU(subMenuPot04,"Pot #4",doNothing,noEvent,noStyle
  ,SUBMENU(selMoistureModeMenuPot04)
  ,FIELD(percentSoilMoisture04,"Current:","%",0,100,0,0,doNothing,noEvent,wrapStyle)
  ,FIELD(moistureOptimumPot04,"Optimum:","%",0,100,10,0,doNothing,noEvent,wrapStyle)
  ,EXIT("<Back")
);

MENU(subMenuIrrigation,"Irrigation",doNothing,noEvent,noStyle
  ,SUBMENU(selPumpModeMenu)
  ,SUBMENU(subMenuPot01)
  ,SUBMENU(subMenuPot02)
  ,SUBMENU(subMenuPot03)
  ,SUBMENU(subMenuPot04)
  ,EXIT("<Back")
);

MENU(subMenuClimate,"Climate",doNothing,noEvent,noStyle
  ,SUBMENU(selFanModeMenu)
  ,FIELD(tempMaximum,"Max Temp:","C",0,100,1,0,doNothing,noEvent,wrapStyle)
  ,FIELD(humidityMaximum,"Max Hum:","%",0,100,1,0,doNothing,noEvent,wrapStyle)
  //,SUBMENU(subMenuTemperature)
  //,SUBMENU(subMenuHumidity)
  //,SUBMENU(subMenuFan)
  ,EXIT("<Back")
);


SELECT(CLEANINGMODE,selDebugModeMenu,"Cleaning:",doNothing,noEvent,noStyle
  ,VALUE("OFF",false,doNothing,noEvent)
  ,VALUE("ON",true,doNothing,noEvent)
);

SELECT(SERIALDEBUG,selCleaningModeMenu,"Debug:",doNothing,noEvent,noStyle
  ,VALUE("OFF",false,doNothing,noEvent)
  ,VALUE("ON",true,doNothing,noEvent)
);

MENU(subMenuSettings,"Settings",doNothing,noEvent,noStyle
  ,SUBMENU(selDebugModeMenu)
  ,OP("Save Settings",updateSettings,enterEvent)
  ,SUBMENU(selCleaningModeMenu)
  ,EXIT("<Back")
);

MENU(mainMenu,"Main menu",doNothing,noEvent,wrapStyle
  ,SUBMENU(subMenuLight)
  ,SUBMENU(subMenuClimate)
  ,SUBMENU(subMenuIrrigation)
  ,SUBMENU(subMenuSettings)
  //,OP("Alert test",doAlert,enterEvent)
  ,EXIT("Status Screen")
);

MENU_OUTPUTS(out,MAX_DEPTH
  //,NONE
  ,SERIAL_OUT(Serial)
  ,U8X8_OUT(u8x8,{0,0,16,8})
);

NAVROOT(nav,mainMenu,MAX_DEPTH,in,out);//the navigation root object


//result doAlert(eventMask e, prompt &item) {
//  nav.idleOn(alert);
//  return proceed;
//}

//result doAlert(eventMask e, prompt &item);

//// Menu Idle Function
//result idle(menuOut& o,idleEvent e) {
//  switch(e) {
//    case idleStart:
//      u8x8.clearDisplay();
//      //o.print("suspending menu!");
//      break;
//    case idling:
//      switchToStatusScreen();
//      //o.print("suspended...");
//      break;
//    case idleEnd:
//      u8x8.clearDisplay();
//      //o.print("resuming menu.");
//      break;
//  }
//  return proceed;
//}

// Save the menu settings
result updateSettings(eventMask e,prompt& p){
 writeSettings();
 nav.idleOn(settingsSaved);
 return proceed;
}

result settingsSaved(menuOut& o,idleEvent e) {
  if (e==idling) {
    o.setCursor(0,1);
    o.print("Settings Saved!");
    o.setCursor(6,4);
    u8x8.setInverseFont(1);
    o.print("OK!");
    u8x8.setInverseFont(0);
  }
  return proceed;
}

/** -------------------------------------------------------------------------- **/

void setup() {

  /** Set the same baud rate as in ESP-Link (if used)**/
  Serial.begin(115200);

  /** Wait for serial connection to get ready (irrelevant on most hardware except e.g. Arduino Due) **/
  while(!Serial);

  /** set watchdog timer to 4 seconds **/
  wdt_enable(WDTO_8S);

  /** Setup the real time clock **/
  setupRTC();

  setupELClient();

  /** Wait for additional time so the wifi serial connection is established and we catch early errors **/
  delay(5000);

  /** Get time from internet via EL-Client rest call to timeapi.org **/
  internetTime();

  /** Read the settings from the EEPROM **/
  readSettings();

  /** Setup the Arduino Menu **/
  setupArduinoMenu();

  /** Setup the buttons **/
  setupButtons();

  /** Setup the LEDs **/
  setupLEDS();

  /** Setup the fan speed controller **/
  setupFanPWM();

  /** Setup the DHT22 temperature & humidity sensor**/
  setupDHT22();

  /** Setup the relays **/
  setupRelay8();

  /** Setup the water level sensor **/
  setupWaterSensor();

  /** Setup the YF-S201 water flow sensor **/
  setupWaterFlowSensor();

  /** Setup the GY-30 brightness sensor**/
  setupGY30();

  //Serial.println("ARDUINO: setup mqtt lwt");
  //mqtt.lwt("/lwt", "offline", 0, 0); //or mqtt.lwt("/lwt", "offline");

}

#define BUFLEN 1000

void loop() {

  //Serial.println((String)"Start: " + millis());

  /** Poll the arduino menu for user input **/
  nav.poll();

  esp.Process();

  /** Uncomment and set to true if you want to see debug output on the serial console.
   *  This can also be toggled through the menu
   *  This option needs to be set after reading the menu with "nav.poll();" **/
  //bool SERIALDEBUG = true;

  /** Control the status LEDS (indicating the modes for: warning, light, climate, pump) **/
  statusLEDS();

  /** Assign the buttons which water individual plants to the plants **/
  for (int i=0; i<sizeof plantsArray/sizeof plantsArray[0]; i++) {
    momentaryButton(i);
  }

  for (int i=4; i<8; i++) {
    modeSwitchButton(i);
  }

  numberOpenValves();

  readWaterFlow();

  /** Slow down execution speed for most operations **/
  if (millis() - previousMillis1 >= mainLoopIntervalSlow) {

    /** Initialize the timer **/
    previousMillis1 = millis();

    //if (SERIALDEBUG == true)Serial.println(F(""));Serial.println(F("###############################"));Serial.println(F(""));

    if (nav.sleepTask) {
      switchToStatusScreen();
    }

    serialStatus();

    midnightReset();

    /** Get current time **/
    timeRTC();

    /** Get current temperatur & humidity **/
    readDHT22();

    /** Get soil moisture for all sensors **/
    //for (int i=0; i<sizeof plantsArray/sizeof plantsArray[0]; i++) {
    //  readSoilMoisture(i);
    //}
    readSoilMoistureNEW();

    /** Get current brightness **/
    readGY30();

    /** Check if there is water in the tank **/
    waterState();

    /** Let magic handle the light **/
    autoLight();

    /** Let magic handle the fan **/
    autoFan();

    if (pumpManual == false) {

      if (CLEANINGMODE == false) {
        /** Let magic handle the pump **/
        //for (int i=0; i<sizeof plantsArray/sizeof plantsArray[0]; i++) {
          autoIrrigation();
        //}

        /** Let magic handle the valves **/
        dryPlants = 0;
        for (int i=0; i<sizeof plantsArray/sizeof plantsArray[0]; i++) {
          openValves(i);
        }
      } else if ((CLEANINGMODE == true) && (digitalRead(waterStatePin) == HIGH)){
        // open all valves and pump
        digitalWrite(relay1, HIGH);
        pumpIsOn = true;
        digitalWrite(valve1, LOW);
        digitalWrite(valve2, LOW);
        digitalWrite(valve3, LOW);
        digitalWrite(valve4, LOW);
        digitalWrite(ledIrrigationPlant1, HIGH);
        digitalWrite(ledIrrigationPlant2, HIGH);
        digitalWrite(ledIrrigationPlant3, HIGH);
        digitalWrite(ledIrrigationPlant4, HIGH);
      }
    }

    /** Get NTP time every hour **/
    if (millis() - previousMillis2 >= 3600000) {
      /** Initialize the timer **/
      previousMillis2 = millis();
      internetTime();
    }

    publishMQTT();

    MQTTReset();
  }


  //Serial.println((String)"End: " + millis());

  /** Reset watchdog **/
  wdt_reset();
}

void reboot() {
  Serial.print(F("oO, we are resetting, this ain't good..."));
  wdt_disable();
  wdt_enable(WDTO_15MS);
  while (1) {}
}

void(* resetFunc) (void) = 0;

void midnightReset(){

  if (today != dateToday){

    if (SERIALDEBUG == true){
      Serial.print(F("MidnightReset -> Today: ")); Serial.print(today);Serial.print(F(" | dateToday: ")); Serial.println(dateToday);
    }

    today = dateToday;
    daysRunning++;

    // Reset
    wpdValve1 = ((wpdValve1 * daysRunning + waterValve1) / daysRunning);
    //waterValve1 = 0.0;
    wpdValve2 = ((wpdValve2 * daysRunning + waterValve2) / daysRunning);
    //waterValve2 = 0.0;
    wpdValve3 = ((wpdValve3 * daysRunning + waterValve3) / daysRunning);
    //waterValve3 = 0.0;
    wpdValve4 = ((wpdValve4 * daysRunning + waterValve4) / daysRunning);
    //waterValve4 = 0.0;
    wpdTotal = ((wpdTotal * daysRunning + waterTotal) / daysRunning);
    //waterTotal = 0.0;
  }
}

// Callback made from esp-link to notify of wifi status changes
// Here we print something out and set a global flag
void wifiCb(void *response) {
  ELClientResponse *res = (ELClientResponse*)response;
  if (res->argc() == 1) {
    uint8_t status;
    res->popArg(&status, 1);

    if(status == STATION_GOT_IP) {
      Serial.println("WIFI CONNECTED");
      wifiConnected = true;
    } else {
      Serial.print("WIFI NOT READY: ");
      Serial.println(status);
      wifiConnected = false;
    }
  }
}

void internetTime(){
  // process any callbacks coming from esp_link

  //delay(5000);

  uint32_t t = cmd.GetTime();
  Serial.print("Adjusted time from NTP to: "); Serial.println(t);
  rtc.adjust(t);
//  if (t) {
//
//    if (SERIALDEBUG == true){
//
//    }
//  }

//  // if we're connected make an HTTP request
//  if(wifiConnected) {
//    rest.get("/");
//    //rest.get("/temperature?id=1275339&appid=15373f8c0b06b6e66e6372db065c4e46");
//    //rest.get("/countryCode?lat=47.03&lng=10.2&username=demo&style=full");
//
//    char response[BUFLEN];
//
//    memset(response, 0, BUFLEN);
//    uint16_t code = rest.waitResponse(response, BUFLEN);
//    if(code == HTTP_STATUS_OK){
//      Serial.println("ARDUINO: GET successful:");
//      Serial.println(response);
//    } else {
//      Serial.print("ARDUINO: GET failed: ");
//      Serial.println(code);
//    }
//    //delay(1000);
//  }
}

void publishMQTT(){

  //Serial.print("publishMQTT: ");Serial.println(connected);

  if (connected && (millis()-last) > mqttPublishInterval) {
    char buf[20];

    //mqtt.publish("/MakerGarden/settings/rtctime", dateNow);

    //itoa(lphValve1, buf, 10);
    //dtostrf(waterValve1, 6, 4, buf);
    //mqtt.publish("/MakerGarden/valves/0/water", buf);

    //itoa(lphValve2, buf, 10);
    //dtostrf(waterValve2, 6, 4, buf);
    //mqtt.publish("/MakerGarden/valves/1/water", buf);

    //itoa(lphValve3, buf, 10);
    //dtostrf(waterValve3, 6, 4, buf);
    //mqtt.publish("/MakerGarden/valves/2/water", buf);

    //itoa(lphValve4, buf, 10);
    //dtostrf(waterValve4, 6, 4, buf);
    //mqtt.publish("/MakerGarden/valves/3/water", buf);

    dtostrf(percentSoilMoisture01, 6, 4, buf);
    mqtt.publish("/MakerGarden/plants/0/soilmoisture", buf);

    //itoa(lphValve2, buf, 10);
    dtostrf(percentSoilMoisture02, 6, 4, buf);
    mqtt.publish("/MakerGarden/plants/1/soilmoisture", buf);

    //itoa(lphValve3, buf, 10);
    dtostrf(percentSoilMoisture03, 6, 4, buf);
    mqtt.publish("/MakerGarden/plants/2/soilmoisture", buf);

    //itoa(lphValve4, buf, 10);
    dtostrf(percentSoilMoisture04, 6, 4, buf);
    mqtt.publish("/MakerGarden/plants/3/soilmoisture", buf);

    dtostrf(lphValve1, 6, 4, buf);
    mqtt.publish("/MakerGarden/valves/0/waterLPH", buf);

    //itoa(lphValve2, buf, 10);
    dtostrf(lphValve2, 6, 4, buf);
    mqtt.publish("/MakerGarden/valves/1/waterLPH", buf);

    //itoa(lphValve3, buf, 10);
    dtostrf(lphValve3, 6, 4, buf);
    mqtt.publish("/MakerGarden/valves/2/waterLPH", buf);

    //itoa(lphValve4, buf, 10);
    dtostrf(lphValve4, 6, 4, buf);
    mqtt.publish("/MakerGarden/valves/3/waterLPH", buf);

    //dtostrf(waterTotal, 6, 4, buf);
    //mqtt.publish("/MakerGarden/valves/water", buf);

    dtostrf(valueTemperature01, 3, 2, buf);
    mqtt.publish("/MakerGarden/climate/temperature", buf);

    dtostrf(valueHumidity01, 4, 2, buf);
    mqtt.publish("/MakerGarden/climate/humidity", buf);

    itoa(valueBrightness01, buf, 10);
    mqtt.publish("/MakerGarden/climate/brightness", buf);

    last = millis();
  }
}

// Callback when MQTT is connected
void mqttConnected(void* response) {
  Serial.println("MQTT connected!");
  //mqtt.subscribe("MakerGarden/valves/0/water");
  //mqtt.subscribe("MakerGarden/valves/1/water");
  //mqtt.subscribe("/hello/world/#");
  //mqtt.subscribe("/esp-link/2", 1);
  //mqtt.publish("/esp-link/0", "test1");

  connected = true;
}

void MQTTReset(){
  if (connected == false) {
    Serial.print("MQTT NOT CONNECTED - reconnecting in: ");Serial.print((resetTimer - (millis() - previousResetTimer)) / 1000);Serial.println("s");
    if (millis() - previousResetTimer >= resetTimer) {
      Serial.println("MQTT SOFT RESET (standard)");
      previousResetTimer = millis();
      //softwareReset::standard();
      reboot();
    }
  }
}

// Callback when MQTT is disconnected
void mqttDisconnected(void* response) {
  Serial.println("MQTT disconnected");
  connected = false;

  // Resync on disconnect
  //resetCb();
  //resetFunc();
  //softwareReset::standard();

  // int Status = mqtt.state();
  //
  // switch (Status){
  //   case -4:
  //     if (SERIALDEBUG == true)(F("Connection timeout"));
  //     break;
  //
  //   case -3:
  //     if (SERIALDEBUG == true)(F("Connection lost"));
  //     break;
  //
  //   case -2:
  //     if (SERIALDEBUG == true)(F("Connect failed"));
  //     break;
  //
  //   case -1:
  //     if (SERIALDEBUG == true)(F("Disconnected"));
  //     break;
  //
  //   case 1:
  //     if (SERIALDEBUG == true)(F("Bad protocol"));
  //     break;
  //
  //   case 2:
  //     if (SERIALDEBUG == true)(F("Bad client ID"));
  //     break;
  //
  //   case 3:
  //     if (SERIALDEBUG == true)(F("Unavailable"));
  //     break;
  //
  //   case 4:
  //     if (SERIALDEBUG == true)(F("Bad credentials"));
  //     break;
  //
  //   case 5:
  //     if (SERIALDEBUG == true)(F("Unauthorized"));
  //     break;
  // }

}

// Callback when an MQTT message arrives for one of our subscriptions
void mqttData(void* response) {
  ELClientResponse *res = (ELClientResponse *)response;

  Serial.print("Received: topic=");
  String topic = res->popString();
  Serial.println(topic);

  Serial.print("data=");
  String data = res->popString();
  Serial.println(data);
}

void mqttPublished(void* response) {
  Serial.println("MQTT published");
}

void switchToStatusScreen(){
  /** Wrapper function to notify the user via the serial console that we are in IDLE mode
   *  Otherwise the serial console is blank and the user does not know what to do!
   *  Since this function gets called on every loop, we want to slow down serial output
  **/
  if (millis() - statusScreenMillis >= mainLoopIntervalSlow) {

    statusScreenMillis = millis();

    Serial.println();
    Serial.println(F("System is in idle mode! Press '*' followed by the 'ENTER' key to see the menu."));
  }

  statusScreenOutput();

}

void statusScreenOutput(){
  u8x8.setInverseFont(1);
  u8x8.drawString(0,0,dateNowShort);
  u8x8.setInverseFont(0);
  u8x8.drawString(0,1,"T: ");u8x8.setCursor(3, 1);u8x8.print(valueTemperature01);u8x8.print("C (");u8x8.print(tempMaximum);;u8x8.print("C)");
  u8x8.drawString(0,2,"H: ");u8x8.setCursor(3, 2);u8x8.print(valueHumidity01);u8x8.print("% (");u8x8.print(humidityMaximum);;u8x8.print("%)");
  u8x8.drawString(0,3,"B: ");u8x8.setCursor(3, 3);u8x8.print(valueBrightness01);u8x8.print("Lux ");
  u8x8.drawString(0,4,"P1: ");u8x8.setCursor(3, 4);u8x8.print(percentSoilMoisture01);u8x8.print("% (");u8x8.print(moistureOptimumPot01);u8x8.print("%)");
  u8x8.drawString(0,5,"P2: ");u8x8.setCursor(3, 5);u8x8.print(percentSoilMoisture02);u8x8.print("% (");u8x8.print(moistureOptimumPot02);u8x8.print("%)");
  u8x8.drawString(0,6,"P3: ");u8x8.setCursor(3, 6);u8x8.print(percentSoilMoisture03);u8x8.print("% (");u8x8.print(moistureOptimumPot03);u8x8.print("%)");
  u8x8.drawString(0,7,"P4: ");u8x8.setCursor(3, 7);u8x8.print(percentSoilMoisture04);u8x8.print("% (");u8x8.print(moistureOptimumPot04);u8x8.print("%)");
  //nav.idleChanged=true;
}

void writeSettings(){
  /** Function which writes our menu settings values to the EEPROM **/
  settingsArray[0] = selFanMode;
  settingsArray[1] = selPumpMode;
  settingsArray[2] = selLightMode;
  settingsArray[3] = autoLightStartTimeHour;
  settingsArray[4] = autoLightEndTimeHour;
  settingsArray[5] = tempMaximum;
  settingsArray[6] = humidityMaximum;
  settingsArray[7] = selMoistureModePot01;
  settingsArray[8] = moistureOptimumPot01;
  settingsArray[9] = selMoistureModePot02;
  settingsArray[10] = moistureOptimumPot02;
  settingsArray[11] = selMoistureModePot03;
  settingsArray[12] = moistureOptimumPot03;
  settingsArray[13] = selMoistureModePot04;
  settingsArray[14] = moistureOptimumPot04;
  settingsArray[15] = SERIALDEBUG;
  settingsArray[16] = autoLightStartTimeMinute;
  settingsArray[17] = autoLightEndTimeMinute;

  // Go back to first address in EEPROM
  eepromAddr = 0;

  // This loop goes through the settingsArray[] one by one
  for (int i=0; i<sizeof settingsArray/sizeof settingsArray[0]; i++) {
    int s = settingsArray[i];
    Serial.println((String)"Storing " + s + " at " + eepromAddr);
    EEPROM.write(eepromAddr, s);
    eepromAddr = eepromAddr + 1;

    // Make sure we don't more to the EEPROM than it can store!
    if (eepromAddr == EEPROM.length()) {
      Serial.println("EEPROM BUFFER OVERRUN");
      eepromAddr = 0;
    }
  }
}

void readSettings(){
  int address = 0;
  byte value;

  for (int i=0; i<sizeof settingsArray/sizeof settingsArray[0]; i++) {
    value = EEPROM.read(address);
    settingsArray[i] = value;
    Serial.println((String)"Reading " + value + " at " + address);
    address = address + 1;
  }

  selFanMode = settingsArray[0];
  selPumpMode = settingsArray[1];
  selLightMode = settingsArray[2];
  autoLightStartTimeHour = settingsArray[3];
  autoLightEndTimeHour = settingsArray[4];
  tempMaximum = settingsArray[5];
  humidityMaximum = settingsArray[6];
  selMoistureModePot01 = settingsArray[7];
  moistureOptimumPot01 = settingsArray[8];
  selMoistureModePot02 = settingsArray[9];
  moistureOptimumPot02 = settingsArray[10];
  selMoistureModePot03 = settingsArray[11];
  moistureOptimumPot03 = settingsArray[12];
  selMoistureModePot04 = settingsArray[13];
  moistureOptimumPot04 = settingsArray[14];
  SERIALDEBUG = settingsArray[15];
  autoLightStartTimeMinute = settingsArray[16];
  autoLightEndTimeMinute = settingsArray[17];

}

void statusLEDS(){
  /** Status LED Control
   *  This function reads gets the settings as read from EEPROM or - if changed during runtime - from the arduino menu
   *
   *  AUTO --> LED is "breathing"
   *  OFF --> LED is off
   *  ON --> LED is on
  **/

  if (selPumpMode == 0){
    ledBreath(ledWater);
  } else if (selPumpMode == 1){
    digitalWrite(ledWater, LOW);
  } else if  (selPumpMode == 2){
    digitalWrite(ledWater, HIGH);
  }

  if (selFanMode == 0){
    ledBreath(ledClimate);
  } else if (selFanMode == 1){
    digitalWrite(ledClimate, LOW);
  } else if  (selFanMode == 2){
    digitalWrite(ledClimate, HIGH);
  }

  if (selLightMode == 0){
    ledBreath(ledLight);
  } else if (selLightMode == 1){
    digitalWrite(ledLight, LOW);
  } else if  (selLightMode == 2){
    digitalWrite(ledLight, HIGH);
  }
}

void openValves(int plantX){

  getPlantVariables(plantX);

  if ((selMoistureModePotX == 0) && (percentSoilMoistureX < moistureOptimumPotX)){

    runPump = true;
    dryPlants++;

    // Only open the valve if pump is already running.
    // This makes sure the valves don't stay on for long time (fire hazard!), e.g. when the tank is empty while plants are dry.
    if ((hasWater == true) && (pumpIsOn == true)){

//      if (SERIALDEBUG == true){
//        Serial.print(F("WARNING: Soil of plant #"));
//        Serial.print(plantX);
//        Serial.print(F(" is dry! --> Opening valve #"));
//        Serial.println(valveX);
//      }

      // Open the valve
      digitalWrite(valveX, LOW);
      digitalWrite(ledIrrigationPlantX, HIGH);
    }
  }
}

void numberOpenValves(){
  valvesOpen = 0;

  if (digitalRead(valve1) == LOW){
    valve1Open = true;
    valvesOpen++;
  } else {
    valve1Open = false;
  }
  if (digitalRead(valve2) == LOW){
    valve2Open = true;
    valvesOpen++;
  } else {
    valve2Open = false;
  }
  if (digitalRead(valve3) == LOW){
    valve3Open = true;
    valvesOpen++;
  } else {
    valve3Open = false;
  }
  if (digitalRead(valve4) == LOW){
    valve4Open = true;
    valvesOpen++;
  } else {
    valve4Open = false;
  }

  // This immediately switches off the pump if no plants are dry and if we are
  // NOT in manual mode.
  // This helps with fluctuating moisture values around the threshold value

  //if (pumpManual == false){
    if ((valvesOpen == 0) && (pumpIsOn == true)){
      digitalWrite(relay1,LOW);
      pumpIsOn = false;
      relay1CameOnX = 0;
      relay1CameOff = millis();
      if (SERIALDEBUG == true){
        Serial.println("Not in pumpManual mode. No valves open but pump is on. Turning off.");
      }
    }
  //}
}

void readSoilMoistureNEW(){

  soilMoistureValue01 = analogRead(soilMoistureSensor1Pin);
  percentSoilMoisture01 = (100 - (soilMoistureValue01 - waterValue) / soilMoistureCoefficient);
  soilMoistureValue02 = analogRead(soilMoistureSensor2Pin);
  percentSoilMoisture02 = (100 - (soilMoistureValue02 - waterValue) / soilMoistureCoefficient);
  soilMoistureValue03 = analogRead(soilMoistureSensor3Pin);
  percentSoilMoisture03 = (100 - (soilMoistureValue03 - waterValue) / soilMoistureCoefficient);
  soilMoistureValue04 = analogRead(soilMoistureSensor4Pin);
  percentSoilMoisture04 = (100 - (soilMoistureValue04 - waterValue) / soilMoistureCoefficient);

}

void timeRTC(){
  DateTime now = rtc.now();
  sprintf(dateNow, "%02d/%02d/%02d %02d:%02d:%02d", now.year(),now.month(),now.day(),now.hour(),now.minute(),now.second());
  sprintf(dateNowShort, "%02d/%02d/%02d %02d:%02d", now.year(),now.month(),now.day(),now.hour(),now.minute());
  dateToday = now.day();
  today = dateToday;
  //today[0] = dateToday[0];
  //today[1] = dateToday[1];
  //if (SERIALDEBUG == true)Serial.println(dateNow);
}

void ledBreath(byte led){

  // Brightness Controll Hack
  // Pin of Water is 10
  float ledOffset = 0.0;
  if (led == 10) {
    ledOffset = 80;
  // Pin of climate is 11
  } else if (led == 11) {
    ledOffset = 60;
  }

  //float val = (exp(sin(millis()/2000.0*PI)) - 0.36787944)*108.0; // TOO BRIGHT
  //float val = (exp(sin(millis()/2000.0*PI)) - 0.36787944)*5;
  float val = (exp(sin(millis()/2000.0*PI)) - 0.36787944) * (108 - ledOffset);
  analogWrite(led, val);

  //if (SERIALDEBUG == true){
  //  Serial.print(F("ledBreathe LED #"));
  //  Serial.print(led);
  //  Serial.print(F("val: "));
  //  Serial.println(val);
  //}


}

void ledBlink(byte led){
  unsigned long ledCurrentMillis = millis();

  if (ledCurrentMillis - ledPreviousMillis >= ledBlinkInterval) {
    // save the last time you blinked the LED
    ledPreviousMillis = ledCurrentMillis;

    // if the LED is off turn it on and vice-versa:
    if (ledState == LOW) {
      ledState = HIGH;
    } else {
      ledState = LOW;
    }

    // set the LED with the ledState of the variable:
    digitalWrite(led, ledState);
  }
}

void momentaryButton(int momentaryButtonX){

  switch (momentaryButtonX) {
    case 0:
      buttonX = digitalRead(button1Pin);
      momentaryButtonXActive = momentaryButton1Active;
      valveX = valve1;
      relayX = relay1;
      ledIrrigationPlantX = ledIrrigationPlant1;
      break;

    case 1:
      buttonX = digitalRead(button2Pin);
      momentaryButtonXActive = momentaryButton2Active;
      valveX = valve2;
      relayX = relay1;
      ledIrrigationPlantX = ledIrrigationPlant2;
      break;

    case 2:
      buttonX = digitalRead(button3Pin);
      momentaryButtonXActive = momentaryButton3Active;
      valveX = valve3;
      relayX = relay1;
      ledIrrigationPlantX = ledIrrigationPlant3;
      break;

    case 3:
      //buttonX = digitalRead(button3Pin);
      buttonX = digitalRead(button4Pin);
      momentaryButtonXActive = momentaryButton4Active;
      valveX = valve4;
      relayX = relay1;
      ledIrrigationPlantX = ledIrrigationPlant4;
      break;
  }

  // check if the pushbutton is pressed.
  if (buttonX == HIGH) {

    //if (SERIALDEBUG == true){
    //  Serial.print(F("Momentary Button #"));
    //  Serial.print(momentaryButtonX);
    //  Serial.print(F(" / Active or not?: "));
    //  Serial.println(momentaryButtonXActive);
    //}

    // This is the button PRESS event

    if (momentaryButtonXActive == false){

      if (momentaryButtonX == 0){
        momentaryButton1Active = true;
      } else if (momentaryButtonX == 1){
        momentaryButton2Active = true;
      } else if (momentaryButtonX == 2){
        momentaryButton3Active = true;
      } else if (momentaryButtonX == 3){
        momentaryButton4Active = true;
      }

      if (hasWater == true){
        digitalWrite(valveX,LOW);
        digitalWrite(relayX,HIGH);
        digitalWrite(ledIrrigationPlantX, HIGH);
        pumpIsOn = true;
        pumpManual = true;
        relay1CameOnX = millis();
        relay1CameOff = 0;
      }
    }

    if (SERIALDEBUG == true){
      Serial.print(F("Momentary Button #"));
      Serial.print(momentaryButtonX);
      Serial.print(F(" PRESSED"));
      Serial.print(F(" -> Active : "));
      Serial.print(momentaryButtonXActive);
      Serial.print(F("pumpManual: "));
      Serial.println(pumpManual);
    }

  } else {

    if (momentaryButtonXActive == true) {

      if (momentaryButtonX == 0){
        momentaryButton1Active = false;
      } else if (momentaryButtonX == 1){
        momentaryButton2Active = false;
      } else if (momentaryButtonX == 2){
        momentaryButton3Active = false;
      } else if (momentaryButtonX == 3){
        momentaryButton4Active = false;
      }
      digitalWrite(valveX,HIGH);
      digitalWrite(relayX,LOW);
      digitalWrite(ledIrrigationPlantX, LOW);
      pumpIsOn = false;
      pumpManual = false;
      relay1CameOnX = 0;
      relay1CameOff = millis();
    }

   //if (SERIALDEBUG == true){
   //  Serial.print(F("Momentary Button #"));
   //  Serial.print(momentaryButtonX);
   //  Serial.print(F(" NOT PRESSED"));
   //  Serial.print(F(" -> Active : "));
   //  Serial.println(buttonXActive);
   //}
  }
}

void modeSwitchButton(int modeSwitchButtonX){

  switch (modeSwitchButtonX) {
    case 4:
      buttonX = digitalRead(button5Pin);
      buttonXActive = button5Active;
      buttonXLongPressActive = button5LongPressActive;
      break;

    case 5:
      buttonX = digitalRead(button6Pin);
      buttonXActive = button6Active;
      buttonXLongPressActive = button6LongPressActive;
      break;

    case 6:
      buttonX = digitalRead(button7Pin);
      buttonXActive = button7Active;
      buttonXLongPressActive = button7LongPressActive;
      break;

    case 7:
      buttonX = digitalRead(button8Pin);
      buttonXActive = button8Active;
      buttonXLongPressActive = button8LongPressActive;
      break;
  }

  if (buttonX == HIGH) {

    Serial.print(F("START Button #"));
    Serial.print(modeSwitchButtonX);
    Serial.print(F(" / buttonXActive: "));
    Serial.println(buttonXActive);

    // This is the button PRESS event
    if (buttonXActive == false) {

      // Do stuff WHILE button is pressed ...

      if (modeSwitchButtonX == 4){
        if (buttonXActive == false) {
          button5Active = true;
          button5Timer = millis();
        }
      } else if (modeSwitchButtonX == 5){
        if (buttonXActive == false) {
          button6Active = true;
          button6Timer = millis();
        }
     } else if (modeSwitchButtonX == 6){
        if (buttonXActive == false) {
          button7Active = true;
          button7Timer = millis();
        }
      } else if (modeSwitchButtonX == 7){
        if (buttonXActive == false) {
          button8Active = true;
          button8Timer = millis();
        }
      }
    }

    if (modeSwitchButtonX == 4){
      if ((millis() - button5Timer > button5LongPressTime) && (button5LongPressActive == false)) {
        button5LongPressActive = true;
        //button5Timer = millis();
      }
    } else if (modeSwitchButtonX == 5){
      if ((millis() - button6Timer > button6LongPressTime) && (button6LongPressActive == false)) {
        button6LongPressActive = true;
        //button5Timer = millis();

        if ((selLightMode == 1) || (selLightMode == 2)){
          selLightMode = 0;
        } else {
          selLightMode = 2;
        }
      }
    } else if (modeSwitchButtonX == 6){
      if ((millis() - button7Timer > button7LongPressTime) && (button7LongPressActive == false)) {
        button7LongPressActive = true;
        //button6Timer = millis();
        if ((selFanMode == 1) || (selFanMode == 2)){
          selFanMode = 0;
        } else {
          selFanMode = 2;
        }
      }
    } else if (modeSwitchButtonX == 7){
      if ((millis() - button8Timer > button8LongPressTime) && (button8LongPressActive == false)) {
        button8LongPressActive = true;
        //button7Timer = millis();

        if ((selPumpMode == 1) || (selPumpMode == 2)){
          selPumpMode = 0;
        } else {
          selPumpMode = 2;
        }
      }
    }
  } else {
    // This is the button RELEASE event
    if (buttonXActive == true) {

      //buttonXActive = false;

      if (buttonXLongPressActive == true) {

        //buttonXLongPressActive = false;

        if (modeSwitchButtonX == 4){
          button5LongPressActive = false;
        } else if (modeSwitchButtonX == 5){
          button6LongPressActive = false;
        } else if (modeSwitchButtonX == 6){
          button7LongPressActive = false;
        } else if (modeSwitchButtonX == 7){
          button8LongPressActive = false;
        }
      } else {
        // Short-Press Release Action here!

        if (modeSwitchButtonX == 4){
          if (button5State == "OFF"){
            button5State = "ON";
          } else {
            button5State = "OFF";
          }
        } else if (modeSwitchButtonX == 5){
          if (button6State == "OFF"){
            selLightMode = 2;
            button6State = "ON";
          } else {
            selLightMode = 1;
            button6State = "OFF";
          }
        } else if (modeSwitchButtonX == 6){
          if (button7State == "OFF"){
            selFanMode = 2;
            button7State = "ON";
          } else {
            selFanMode = 1;
            button7State = "OFF";
          }
        } else if (modeSwitchButtonX == 7){
          if (button8State == "OFF"){
            selPumpMode = 2;
            button8State = "ON";
          } else {
            selPumpMode = 1;
            button8State = "OFF";
          }
        }
      }

      if (modeSwitchButtonX == 4){
        button5Active = false;
        //button5Timer = millis();
      } else if (modeSwitchButtonX == 5){
        button6Active = false;
        //button5Timer = millis();
      } else if (modeSwitchButtonX == 6){
        button7Active = false;
        //button6Timer = millis();
      } else if (modeSwitchButtonX == 7){
        button8Active = false;
        //button7Timer = millis();
      }
    }
  }
}

void readDHT22(){

  // Get temperature event and print its value.
  sensors_event_t event;
  dht.temperature().getEvent(&event);

  if (isnan(event.temperature)) {
    Serial.println(F("Error reading temperature! Will reboot using WDT"));
    reboot();
  } else {
    valueTemperature01 = (event.temperature);
    //if (SERIALDEBUG == true)Serial.println((String)"T: " + valueTemperature01 + "°C");
  }

  // Get humidity event and print its value.
  dht.humidity().getEvent(&event);

  if (isnan(event.relative_humidity)) {
    Serial.println(F("Error reading humidity! Will reboot using WDT"));
    reboot();
  } else {
    valueHumidity01 = event.relative_humidity;
    //if (SERIALDEBUG == true)Serial.println((String)"H: " + valueHumidity01 + "%");
  }
}

void getPlantVariables(int plantX){

  percentSoilMoistureX = 0;
  selMoistureModePotX = 0;
  moistureOptimumPotX = 0;
  soilMoistureValueX = 0;
  valveX = 99;
  valveXState = HIGH;

  switch (plantX) {
    case 0:
      valveX = valve1;
      valveXState = digitalRead(valve1);
      waterValveX = waterValve1;
      wpdValveX = wpdValve1;
      soilMoistureValueX = soilMoistureValue01;
      percentSoilMoistureX = percentSoilMoisture01;
      moistureOptimumPotX = moistureOptimumPot01;
      selMoistureModePotX = selMoistureModePot01;
      ledIrrigationPlantX = ledIrrigationPlant1;
      break;

    case 1:
      valveX = valve2;
      valveXState = digitalRead(valve2);
      waterValveX = waterValve2;
      wpdValveX = wpdValve2;
      soilMoistureValueX = soilMoistureValue02;
      percentSoilMoistureX = percentSoilMoisture02;
      moistureOptimumPotX = moistureOptimumPot02;
      selMoistureModePotX = selMoistureModePot02;
      ledIrrigationPlantX = ledIrrigationPlant2;
      break;

    case 2:
      valveX = valve3;
      valveXState = digitalRead(valve3);
      waterValveX = waterValve3;
      wpdValveX = wpdValve3;
      soilMoistureValueX = soilMoistureValue03;
      percentSoilMoistureX = percentSoilMoisture03;
      moistureOptimumPotX = moistureOptimumPot03;
      selMoistureModePotX = selMoistureModePot03;
      ledIrrigationPlantX = ledIrrigationPlant3;
      break;

    case 3:
      valveX = valve4;
      valveXState = digitalRead(valve4);
      waterValveX = waterValve4;
      wpdValveX = wpdValve4;
      soilMoistureValueX = soilMoistureValue04;
      percentSoilMoistureX = percentSoilMoisture04;
      selMoistureModePotX = selMoistureModePot04;
      moistureOptimumPotX = moistureOptimumPot04;
      ledIrrigationPlantX = ledIrrigationPlant4;
      break;
  }

  if (SERIALDEBUG == true){
    Serial.print(F("Plant #"));
    Serial.print(plantX);
    Serial.print(F(" (Valve #"));
    Serial.print(valveX);
    Serial.print(F(" / State: "));
    Serial.print(valveXState);
    Serial.print(F(" / Water L: "));
    Serial.print(waterValveX, 4);
    Serial.print(F(" / WPD: "));
    Serial.print(wpdValveX, 4);
    Serial.print(F(" ) / Current: "));
    Serial.print(percentSoilMoistureX);
    Serial.print(F(" (RAW: "));
    Serial.print(soilMoistureValueX);
    Serial.print(F(") / Optimum: "));
    Serial.println(moistureOptimumPotX);
  }

}

void waterState(){
  if (digitalRead(waterStatePin) == HIGH){
    hasWater = true;
    digitalWrite(ledWarn,LOW);
  } else {
    hasWater = false;
    ledBlink(ledWarn);
    if (SERIALDEBUG == true){Serial.println(F("WARNING: WATER IS EMPTY "));}
  }
}

void autoIrrigation(){
  if (selPumpMode == 0){

    autoIrrigationNextRun = ((long)(irrigationWaitPeriod - (millis() - relay1CameOff))/1000);

    // If the pump is already running, check how long already and turn it off and close all valves after the irrigationDuration timeout
    if (digitalRead(relay1) == HIGH){

      if (SERIALDEBUG == true)Serial.println((String)"Pump01 ON (since: " + (millis()-relay1CameOnX) + "ms)");

      // If the pump has been on for x seconds turn it off again
      if(millis()-relay1CameOnX > irrigationDuration){
        digitalWrite(valve1,HIGH);
        digitalWrite(ledIrrigationPlant1, LOW);
        digitalWrite(valve2,HIGH);
        digitalWrite(ledIrrigationPlant2, LOW);
        digitalWrite(valve3,HIGH);
        digitalWrite(ledIrrigationPlant3, LOW);
        digitalWrite(valve4,HIGH);
        digitalWrite(ledIrrigationPlant4, LOW);
        digitalWrite(relay1,LOW);
        pumpIsOn = false;
        if (SERIALDEBUG == true)Serial.println(F("Pump01: TURNING OFF"));
        relay1CameOnX = 0;
        relay1CameOff = millis();
        autoIrrigationCounter = millis();
      }

    } else {

      if ((millis() - relay1CameOff) > irrigationWaitPeriod){

        if ((hasWater == true) && (runPump == true) && (dryPlants > 0)){

          // If soil is dry, turn on relay1 for pump
          digitalWrite(relay1, HIGH);
          relay1CameOnX = millis();
          relay1CameOff = 0;
          pumpIsOn = true;
          //if (SERIALDEBUG == true)Serial.println(F("Pump01: TURNING ON"));
          runPump = false;
        }

      }

    }

  } else if (selPumpMode == 2) {
    if (hasWater){
      analogWrite(ledWater, HIGH);
      //if (SERIALDEBUG == true)Serial.println(F("PumpMode: ON"));
      digitalWrite(relay1, HIGH);
      pumpIsOn = true;
    }

  } else if (selPumpMode == 1) {
    analogWrite(ledWater, LOW);
    //if (SERIALDEBUG == true)Serial.println(F("PumpMode: OFF"));
    digitalWrite(relay1, LOW);
    pumpIsOn = false;
  }
}

void autoFan(){

  if (selFanMode == 0) {

    static String tStart;
    static String tEnd;
    byte h = rtc.now().hour();
    byte m = rtc.now().minute();
    int t = (h * 60) + m;

    if(digitalRead(relay2) == HIGH){

      if((valueTemperature01 < tempMaximum) && (valueHumidity01 < humidityMaximum )){
        tEnd = dateNow;
        digitalWrite(relay2,LOW);
        if (SERIALDEBUG == true)Serial.println((String)"Fan01: Turning OFF (" + tEnd + ")");
        relay2CameOn = 0;
      }

    } else {

      // Calculate difference between optimum humidity / temperature and actual humidity / temperature.
      // If the difference is small, we run the fan at low speed, if the difference is big, we run at high speed!
      float tempDiff = (tempMaximum / valueTemperature01);
      float humDiff = (humidityMaximum / valueHumidity01);

      //if (SERIALDEBUG == true)Serial.println((String)"tempDiff (" + tempDiff + ")");

      if ((tempDiff < 1) || (humDiff < 1)){
        fanSpeed = 1;
      } else if ((tempDiff > 1) || (humDiff > 1)){
        // Find which difference is lower (aka which value is closer to the maximum threshold) and use this for fanSpeed calculation
        if (tempDiff > humDiff) {
          fanSpeed = (1 / humDiff *255);
        } else {
          fanSpeed = (1 / tempDiff *255);
        }
      } else if ((tempDiff > 2) || (humDiff > 2)){
        fanSpeed = 0;
      }

      if ((valueTemperature01 >= tempMaximum) || (valueHumidity01 >= humidityMaximum )){
        tStart = dateNow;
        digitalWrite(relay2, HIGH);
        //analogWrite(fanSpeedPin, fanSpeed);
        relay2CameOn = millis();
        if (SERIALDEBUG == true){
          Serial.print((String)"Fan01: Turning ON (" + tStart + ")");
          Serial.println((String)"(fanSpeed: " + fanSpeed + ")");
        }
      }
    }
  } else if (selFanMode == 2) {
    analogWrite(ledClimate, HIGH);
    //if (SERIALDEBUG == true)Serial.println(F("FanMode: ON"));
    digitalWrite(relay2, HIGH);

  } else if (selFanMode == 1) {
    analogWrite(ledClimate, LOW);
    //if (SERIALDEBUG == true)Serial.println(F("FanMode: OFF"));
    digitalWrite(relay2, LOW);
  }

}

void autoLight(){

  if (selLightMode == 0){

    static String tStart;
    static String tEnd;
    byte h = rtc.now().hour();
    byte m = rtc.now().minute();
    int t = (h * 60) + m;
    int autoLightStartTime = (autoLightStartTimeHour * 60) + autoLightStartTimeMinute;
    int autoLightEndTime = (autoLightEndTimeHour * 60) + autoLightEndTimeMinute;
    int diff = (autoLightEndTime - autoLightStartTime);

    // If the light is already on...
    if (digitalRead(relay3) == HIGH){

      // Check the current time (t) against our desired END time and turn off the light if needed
      if ((unsigned)(t - autoLightStartTime) >= diff){
        tEnd = dateNow;
        digitalWrite(relay3,LOW);
        if (SERIALDEBUG == true)Serial.println((String)"Light01: Turning OFF (" + tEnd + ")");
      }

    // If the light is off...
    } else {

      // Check the current time (t) against our desired START time and turn on the light if needed
      if ((unsigned)(t - autoLightStartTime) < diff) {
        tStart = dateNow;
        digitalWrite(relay3, HIGH);
      }
    }

  } else if (selLightMode == 2){
    analogWrite(ledLight, HIGH);
    digitalWrite(relay3, HIGH);

  } else if (selLightMode == 1) {
    analogWrite(ledLight, LOW);
    digitalWrite(relay3, LOW);
  }

}

void flowCounter(){
  // Interrupt function
  flow_frequency++;
}

void readWaterFlow(){
  currentTime = millis();
  float flowRate;

  // Every second, calculate and print litres/hour
  if(currentTime >= (cloopTime + 1000)){
    cloopTime = currentTime;

    numberOpenValves();

    // flowRate depends on strength of pump and surface area
    // with a variable number of valves open, the surface area differs
    // Since we run the pump only for a short time each time (irrigationDuration = 5s), during the starting up (first second) the pump is not in full capacity.
    //flowRate = 105 * valvesOpen / 4; // 25 - 100 -> works for 1 -2 valves open
    flowRate = 100.0 * valvesOpen / 6.0; // 20 - 80
    l_hour = (flow_frequency * 60.0 / flowRate); // (Pulse frequency x 60 min) / 7.5Q = flowrate in L/min

    countWater(l_hour);

    countWaterPerValve(l_hour);

    flow_frequency = 0; // Reset Counter
  }
}

void countWaterPerValve(float wpv){

  //getPlantVariables(i);

  numberOpenValves();

  if (digitalRead(valve1) == LOW){
    lphValve1 = wpv / valvesOpen;
    waterValve1 = waterValve1 + (wpv / 3600.0 / valvesOpen);
  } else {
    lphValve1 = 0;
  }
  if (digitalRead(valve2) == LOW){
    lphValve2 = wpv / valvesOpen;
    waterValve2 = waterValve2 + (wpv / 3600.0 / valvesOpen);
  } else {
    lphValve2 = 0;
  }
  if (digitalRead(valve3) == LOW){
    lphValve3 = wpv / valvesOpen;
    waterValve3 = waterValve3 + (wpv / 3600.0 / valvesOpen);
  } else {
    lphValve3 = 0;
  }
  if (digitalRead(valve4) == LOW){
    lphValve4 = wpv / valvesOpen;
    waterValve4 = waterValve4 + (wpv / 3600.0 / valvesOpen);
  } else {
    lphValve4 = 0;
  }

}

void countWater(float wt){

  //if (pumpIsOn == true){
    //waterTotal = waterTotal + literPerHour / 3600;
    waterTotal = waterTotal + wt / 3600.0;
    lphTotal = wt;
  //}
//  if (SERIALDEBUG == true){
//    Serial.print("Water Total: ");
//    Serial.print(waterTotal, 4); // Print litres/hour
//    Serial.print(F(" L (Current Flow Rate: "));
//    Serial.print(wt);
//    Serial.println(F(" L/h)"));
//  }
}

void serialStatus(){
  if (SERIALDEBUG == true){
    Serial.println();
    Serial.println("------------------------------------------------------------------------");
    Serial.println();
    Serial.print(dateNow);Serial.print(" (Days Running: ");Serial.print(daysRunning);Serial.println(")");
    Serial.println();
    Serial.print(F("Temp: "));Serial.print(valueTemperature01);Serial.print(F("°C | "));
    Serial.print(F("Humidity: "));Serial.print(valueHumidity01);Serial.print(F("% | "));
    Serial.print(F("Brightness: "));Serial.print(valueBrightness01);Serial.print(F(" Lux | "));
    Serial.print(F("Pump: "));Serial.print(waterTotal, 4);Serial.print(F(" L (Flow: "));Serial.print(l_hour);Serial.print(F(" L/h) | "));
    Serial.print(F("Irrigation Timer: "));Serial.print(autoIrrigationNextRun);Serial.println(F("s"));
  }
}

void setupELClient(){
  Serial.println("EL-Client starting!");

  // Sync-up with esp-link, this is required at the start of any sketch and initializes the
  // callbacks to the wifi status change callback. The callback gets called with the initial
  // status right after Sync() below completes.
  esp.wifiCb.attach(wifiCb); // wifi status change callback, optional (delete if not desired)

  resetCb();


  // Get immediate wifi status info for demo purposes. This is not normally used because the
  // wifi status callback registered above gets called immediately.
  // esp.GetWifiStatus();
  // ELClientPacket *packet;
  // if ((packet=esp.WaitReturn()) != NULL) {
  //   Serial.print("Wifi status: ");
  //   Serial.println(packet->value);
  // }

  //setupRest();

  //setupMQTT();

}

void resetCb(){
  //bool ok = false;

  Serial.println("Syncing or resyncing Arduino and ESP8266...");

  bool ok;
  do {
    ok = esp.Sync();      // sync up with esp-link, blocks for up to 2 seconds
    if (!ok) Serial.println("EL-Client sync failed!");
  } while(!ok);

  Serial.println("EL-Client synced!");

  setupRest();
  setupMQTT();

}

void setupRest(){
  // Set up the REST client to talk to www.timeapi.org, this doesn't connect to that server,
  // it just sets-up stuff on the esp-link side
  int err = rest.begin("flipsa.de");
  //int err = rest.begin("idiotware.herokuapp.com");
  //int err = rest.begin("api.geonames.org");

  if (err != 0) {
    Serial.print("REST begin failed: ");
    Serial.println(err);
    while(1) ;
  } else {
    Serial.println("EL-REST ready");
  }
}

void setupMQTT(){
  // Set-up callbacks for events and initialize with es-link.
  mqtt.connectedCb.attach(mqttConnected);
  mqtt.disconnectedCb.attach(mqttDisconnected);
  mqtt.publishedCb.attach(mqttPublished);
  mqtt.dataCb.attach(mqttData);
  mqtt.setup();

}

void setupRTC(){
  if (!rtc.begin()) {
    Serial.println(F("Couldn't find RTC"));
    while (1);
  }

  if (!rtc.isrunning()) {
    if (SERIALDEBUG == true)Serial.println(F("RTC is NOT running!"));
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    if (SERIALDEBUG == true)Serial.println(F("RTC adjusted!"));
  }


}

void setupArduinoMenu(){
/** Arduino Menu Library Setup (https://github.com/neu-rah/ArduinoMenu) **/

  //Serial.println("Arduino Menu Library");Serial.flush();
  //nav.idleTask=idle;//point a function to be used when menu is suspended
  //mainMenu[1].enabled=disabledStatus;
  subMenuPot01[1].disable();
  subMenuPot02[1].disable();
  subMenuPot03[1].disable();
  subMenuPot04[1].disable();
  nav.showTitle=true;
  nav.timeOut = 10;
  u8x8.begin();
  u8x8.setFont(u8x8_font_chroma48medium8_r);
  //u8x8.drawString(0,0,"Menu 4.x");
  Timer1.initialize(1000);
  Timer1.attachInterrupt(timerIsr);
  //Serial.println((String)"Accel: " + clickEncoder.getAccelerationEnabled());
}

void setupButtons(){
  pinMode(button1Pin, INPUT_PULLUP);
  pinMode(button2Pin, INPUT_PULLUP);
  pinMode(button3Pin, INPUT_PULLUP);
  pinMode(button4Pin, INPUT_PULLUP);
  pinMode(button5Pin, INPUT_PULLUP);
  pinMode(button6Pin, INPUT_PULLUP);
  pinMode(button7Pin, INPUT_PULLUP);
  pinMode(button8Pin, INPUT_PULLUP);
  pinMode(encBtn, INPUT_PULLUP);
}

void setupWaterFlowSensor(){
  // YF-S201
  pinMode(flowSensorPin, INPUT);
  //digitalWrite(flowSensorPin, HIGH); // Optional Internal Pull-Up
  //attachInterrupt(digitalPinToInterrupt(flowSensorPin), flowCounter, RISING); // Setup Interrupt
  attachInterrupt(0, flowCounter, RISING); // Setup Interrupt
  sei(); // Enable interrupts
  currentTime = millis();
  cloopTime = currentTime;
}

void setupGY30(){
  Wire.beginTransmission(gy30ICAdress);
  Wire.write(0x10); //Genauigkeit des Sensors
  Wire.endTransmission();
}

void readGY30(){
  if (resultGY30() == 2){
    valueBrightness01 = ((buffer[0] << 8) | buffer[1]);
    //if (SERIALDEBUG == true)Serial.println((String)"B: " + valueBrightness01 + " Lx");
  } else {
    if (SERIALDEBUG == true)Serial.println(F("ERROR: Brightness Sensor GY30 NOT AVAILABLE"));
  }
}

int resultGY30(){
  byte value = 0;
  Wire.beginTransmission(gy30ICAdress);
  Wire.requestFrom(gy30ICAdress, 2);
  while(Wire.available()){
    buffer[value] = Wire.read();
    value++;
  }
  Wire.endTransmission();
  return value;
}

void setupWaterSensor(){
  pinMode(waterStatePin, INPUT);
  digitalWrite(waterStatePin, HIGH);
}

void setupLEDS(){
  pinMode(ledWarn, OUTPUT);
  digitalWrite(ledWarn, LOW);
  pinMode(ledLight, OUTPUT);
  digitalWrite(ledLight, LOW);
  pinMode(ledClimate, OUTPUT);
  digitalWrite(ledClimate, LOW);
  pinMode(ledWater, OUTPUT);
  digitalWrite(ledWater, LOW);
  pinMode(ledIrrigationPlant1, OUTPUT);
  digitalWrite(ledIrrigationPlant1, LOW);
  pinMode(ledIrrigationPlant2, OUTPUT);
  digitalWrite(ledIrrigationPlant2, LOW);
  pinMode(ledIrrigationPlant3, OUTPUT);
  digitalWrite(ledIrrigationPlant3, LOW);
  pinMode(ledIrrigationPlant4, OUTPUT);
  digitalWrite(ledIrrigationPlant4, LOW);
  //pinMode(LED_BUILTIN,OUTPUT);
}

void setupFanPWM(){
  pinMode(fanSpeedPin, INPUT);
}

void setupRelay8(){
  // Turn all relays off when we start the Arduino
  digitalWrite(relay1, LOW);
  pinMode(relay1, OUTPUT);
  //relay1CameOff = millis();
  digitalWrite(relay2, LOW);
  pinMode(relay2, OUTPUT);
  digitalWrite(relay3, LOW);
  pinMode(relay3, OUTPUT);

  digitalWrite(valve1, HIGH);
  pinMode(valve1, OUTPUT);
  digitalWrite(valve2, HIGH);
  pinMode(valve2, OUTPUT);
  digitalWrite(valve3, HIGH);
  pinMode(valve3, OUTPUT);
  digitalWrite(valve4, HIGH);
  pinMode(valve4, OUTPUT);
}

void setupDHT22(){

  dht.begin();

  // Print temperature sensor details.
  sensor_t sensor;

  dht.temperature().getSensor(&sensor);
  if (SERIALDEBUG == true){
    Serial.println(F("------------------------------------"));
    Serial.println(F("Temperature"));
    Serial.print  (F("Sensor:       ")); Serial.println(sensor.name);
    Serial.print  (F("Driver Ver:   ")); Serial.println(sensor.version);
    Serial.print  (F("Unique ID:    ")); Serial.println(sensor.sensor_id);
    Serial.print  (F("Max Value:    ")); Serial.print(sensor.max_value); Serial.println(F(" *C"));
    Serial.print  (F("Min Value:    ")); Serial.print(sensor.min_value); Serial.println(F(" *C"));
    Serial.print  (F("Resolution:   ")); Serial.print(sensor.resolution); Serial.println(F(" *C"));
    Serial.println(F("------------------------------------"));
  }

  // Print humidity sensor details.
  dht.humidity().getSensor(&sensor);
  if (SERIALDEBUG == true){
    Serial.println(F("------------------------------------"));
    Serial.println(F("Humidity"));
    Serial.print  (F("Sensor:       ")); Serial.println(sensor.name);
    Serial.print  (F("Driver Ver:   ")); Serial.println(sensor.version);
    Serial.print  (F("Unique ID:    ")); Serial.println(sensor.sensor_id);
    Serial.print  (F("Max Value:    ")); Serial.print(sensor.max_value); Serial.println(F("%"));
    Serial.print  (F("Min Value:    ")); Serial.print(sensor.min_value); Serial.println(F("%"));
    Serial.print  (F("Resolution:   ")); Serial.print(sensor.resolution); Serial.println(F("%"));
    Serial.println(F("------------------------------------"));
  }
  // Set delay between sensor readings based on sensor details.
  //delayDHT22 = sensor.min_delay / 1000;
}
