# MakerGarden

MakerGarden is an umbrella project for full garden-automation based on inexpensive microcontrollers such as Arduino and ESP32 boards.

The first prototype is a fully self-contained and fully automatic greenhouse with sensors (soil moisture, temperature, humidity, light intensity, rain) and actors (lights, water pump / solenoids). It is currently running on an Arduino Mega. Additional battery-powered satellites will be based on ESP32 running MicroPython. This prototype is still under development but has been running stable and without major problems for about 6 months now. 

Stay tuned for more information (and some pictures soon)...
